package niallconroy.swipejobs.api;

import com.google.gson.Gson;
import com.jayway.restassured.response.Response;

public class RestAssuredSerialiser {
	//Simple method to serialise json into a java class
	public static <T> T serialise(Response response, Class<T> clazz) {
		Gson gson = new Gson();
		return (T) gson.fromJson(response.getBody().asString(), clazz);
	}
}
