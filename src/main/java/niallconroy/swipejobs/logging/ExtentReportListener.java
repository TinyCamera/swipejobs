package niallconroy.swipejobs.logging;

import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.internal.IResultListener;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
/*
 * This testng listener user Extent report to generate a nice looking HTML report of the tests run
 */
public class ExtentReportListener implements IResultListener{
	private static final String EXTENT_TEST = "ExtentTest";
	private static final ExtentReports extent = new ExtentReports();
	private static ExtentHtmlReporter htmlReporter; 

	@Override
	public void onFinish(ITestContext arg0) {
		extent.flush();
	}

	@Override
	public void onStart(ITestContext arg0) {
		htmlReporter = new ExtentHtmlReporter("SwipeJobsAPITestReport.html");
		extent.attachReporter(htmlReporter);		
	}

	@Override
	public void onTestFailedButWithinSuccessPercentage(ITestResult arg0) {
	}

	@Override
	public void onTestFailure(ITestResult result) {
		ExtentTest test = (ExtentTest) result.getAttribute(EXTENT_TEST);
		test.assignCategory(result.getMethod().getGroups());
		test.fail(result.getThrowable());
	}

	@Override
	public void onTestSkipped(ITestResult arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onTestStart(ITestResult result) {
		ExtentTest test = extent.createTest(result.getName(),result.getMethod().getDescription());
		result.setAttribute(EXTENT_TEST, test);
	}

	@Override
	public void onTestSuccess(ITestResult result) {
		ExtentTest test = (ExtentTest) result.getAttribute(EXTENT_TEST);
		test.assignCategory(result.getMethod().getGroups());
		test.pass("Passed!");
	}

	@Override
	public void onConfigurationFailure(ITestResult arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onConfigurationSkip(ITestResult arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onConfigurationSuccess(ITestResult arg0) {
		// TODO Auto-generated method stub

	}



}
