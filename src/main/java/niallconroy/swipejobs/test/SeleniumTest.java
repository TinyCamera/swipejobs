package niallconroy.swipejobs.test;


import org.testng.ITestContext;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;

import niallconroy.swipejobs.webdriver.DriverManager;
import niallconroy.swipejobs.webdriver.WebDriverFactory;

public class SeleniumTest extends TestBase{
	/*
	 * All web tests can extent this class. This will initialise DriverManager based on params in the suite file
	 */


	@Parameters({WebDriverFactory.BROWSER_NANE, WebDriverFactory.BROWSER_VERSION,WebDriverFactory.BROWSER_PLATFORM})
	@BeforeSuite
	public void setup(ITestContext context, String browserName, @Optional("") String browserVersion, @Optional("")String browserPlatform) {

		context.setAttribute(WebDriverFactory.BROWSER_NANE, browserName);
		context.setAttribute(WebDriverFactory.BROWSER_VERSION, browserVersion);
		context.setAttribute(WebDriverFactory.BROWSER_PLATFORM, browserPlatform);

		DriverManager.setContext(context);
	}


	@AfterSuite
	public void finish() {
		DriverManager.closeDrivers();
	}
}
