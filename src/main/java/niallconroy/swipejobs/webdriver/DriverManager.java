package niallconroy.swipejobs.webdriver;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.FluentWait;
import org.testng.Assert;
import org.testng.ITestContext;

public class DriverManager {
	
	//keep track of active threads and the webdriver assigned to them
	private static Map<Thread, WebDriver> webDrivers = new ConcurrentHashMap<Thread, WebDriver>();

	private static ITestContext context; 

	/*
	 * DriverManage provide a static refernce to a webDriver that tests can make use of. Furthermore, it initilaise and 
	 * webdriver for each thread running a test, allowing tests to be run in parallel
	 */
	public static WebDriver get() {
		if(!webDrivers.containsKey(Thread.currentThread()) || webDrivers.get(Thread.currentThread())==null) {
			initializeDriver();
		}
		
		return webDrivers.get(Thread.currentThread());
	}
	
	public static FluentWait<WebDriver> shortWait() {
		return getFluentWait(6);
	}

	public static FluentWait<WebDriver> longWait() {
		return getFluentWait(30);
	}
	
	public static void setContext(ITestContext _context) {
		context = _context;
	}
	public static void closeDrivers() {
		webDrivers.values().stream().filter(driver -> driver !=null).forEach(driver -> {
			driver.close();
		});
	}

	private static void initializeDriver() {
		Assert.assertNotNull(context, "DriverManager has not been initilised properly");

		WebDriver webdriver = WebDriverFactory.getWebDriver(context);
		webDrivers.put(Thread.currentThread(),webdriver);
	}
	
	private static FluentWait<WebDriver> getFluentWait(int timeout){
		return new FluentWait<WebDriver>(get()).withTimeout(timeout, TimeUnit.SECONDS).ignoring(StaleElementReferenceException.class);
	}
}

