package niallconroy.swipejobs.webdriver;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.Assert;
import org.testng.ITestContext;

import io.github.bonigarcia.wdm.ChromeDriverManager;
import io.github.bonigarcia.wdm.FirefoxDriverManager;

public class WebDriverFactory {

	public static final String FIREFOX = "firefox";
	public static final String CHROME = "chrome";

	public static final String BROWSER_NANE = "browserName";
	public static final String BROWSER_VERSION = "browserVersion";
	public static final String BROWSER_PLATFORM = "platform";

	public static boolean USE_SELENIUM_GRID = "true".equals(System.getProperty("useSeleniumGrid"));

	public static WebDriver getWebDriver(ITestContext context) {
		String browserName = (String) context.getAttribute(BROWSER_NANE);
		String browserVersion = (String) context.getAttribute(BROWSER_VERSION);
		String platform = (String) context.getAttribute(BROWSER_PLATFORM);

		if(USE_SELENIUM_GRID) {
			return getRemoteWebDriver(browserName, browserVersion, platform);
		}else {
			return getLocalWebDriver(browserName);
		}
		
	}


	private static WebDriver getLocalWebDriver(String browserName) {
		WebDriver webDriver = null;
		switch (browserName) {
		case CHROME:
			ChromeDriverManager.getInstance().setup();
			webDriver = new ChromeDriver();
			break;
		case FIREFOX:
			FirefoxDriverManager.getInstance().setup();
			webDriver = new FirefoxDriver();
			break;
		default:
			Assert.fail(browserName + "is not a valid browser");
			break;
		}

		return webDriver;
	}

	private static RemoteWebDriver getRemoteWebDriver(String browserName, String browserVersion, String platform) {
		/**
		 * Here you could have code to initialise a remote webdriver
		 * and connect to a selenium grid.
		 */
		return null;

	}
}
