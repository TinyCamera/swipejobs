package niallconroy.swipejobs.webdriver.conditions;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;

/**
 * This condition is specifially written to better detect when the ranked job list is finished populating
 * @author Niall
 *
 */
public class WebElementHasClass implements ExpectedCondition<Boolean>{
	WebElement element;
	String className;

	public WebElementHasClass(WebElement element, String className) {
		this.element = element;
		this.className = className; 
	}

	@Override
	public Boolean apply(WebDriver arg0) {
		return element.getAttribute("class").contains(className);
	}

}
