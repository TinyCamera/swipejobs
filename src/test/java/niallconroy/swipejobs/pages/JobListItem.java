package niallconroy.swipejobs.pages;

import org.openqa.selenium.SearchContext;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class JobListItem extends Page {

	@FindBy(css="td")
	WebElement rank;

	@FindBy(css="td:nth-child(2)")
	WebElement jobTitle;

	@FindBy(css="td:nth-child(3)")
	WebElement company;

	@FindBy(css="td:nth-child(4)")
	WebElement billRate;

	public JobListItem(SearchContext context) {
		super(context);
	}
	
	public WebElement getRank() {
		return rank;
	}

	public WebElement getJobTitle() {
		return jobTitle;
	}

	public WebElement getCompany() {
		return company;
	}

	public WebElement getBillRate() {
		return billRate;
	}
	
	
}
