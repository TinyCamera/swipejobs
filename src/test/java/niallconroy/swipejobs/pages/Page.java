package niallconroy.swipejobs.pages;

import org.openqa.selenium.SearchContext;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.DefaultElementLocatorFactory;

import niallconroy.swipejobs.webdriver.DriverManager;

abstract public class Page {
	
	public Page(SearchContext context) {
		PageFactory.initElements(new DefaultElementLocatorFactory(context), this);
	}
	
	public Page() {
		PageFactory.initElements(DriverManager.get(), this);
	}
	
}

