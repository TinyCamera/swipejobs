package niallconroy.swipejobs.pages;

import java.util.ArrayList;
import java.util.List;

import org.assertj.core.api.SoftAssertions;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

import niallconroy.swipejobs.serialise.WorkersApiWorker;
import niallconroy.swipejobs.webdriver.DriverManager;
import niallconroy.swipejobs.webdriver.conditions.WebElementHasClass;
/**
 * Page object for the swipeJobsApi webapp
 * @author Niall
 *
 */
public class SwipeJobsPage extends Page{

	@FindBy(css="input[ng-model='vm.workerId']")
	WebElement workerIdInput;

	@FindBy(css="button[ng-model='vm.buttonSearchWorker']")
	WebElement searchWorkerButton;

	@FindBy(css="input[ng-model='vm.workerInfoName']")
	WebElement workerName;

	@FindBy(css="input[ng-model='vm.workerInfoAge']")
	WebElement workerAge;

	@FindBy(css="input[ng-model='vm.workerInfoEmail']")
	WebElement workerEmail;

	@FindBy(css="input[ng-model='vm.workerInfoPhone']")
	WebElement workerPhone;

	@FindBy(css="input[ng-model='vm.workerInfoRating']")
	WebElement workerRating;

	@FindBy(css="input[ng-model='vm.workerInfoIsActive']")
	WebElement workerIsActive;

	@FindBy(css="#DataTables_Table_0_filter input")
	WebElement jobSerachInput;

	@FindBy(css="#DataTables_Table_0 th")
	WebElement rankTableTitle;

	@FindBy(css="#DataTables_Table_0 th:nth-child(1)")
	WebElement jobTableTitle; 

	@FindBy(css="#DataTables_Table_0 th:nth-child(2)")
	WebElement companyTableTite;

	@FindBy(css="#DataTables_Table_0 th:nth-child(3)")
	WebElement billRateTableTitle;

	@FindBy(css="#DataTables_Table_0 tbody tr")
	List<WebElement> jobListWebElements;

	@FindBy(css="#DataTables_Table_0 tbody tr td")
	WebElement emptyTableMessage;

	List<JobListItem> jobListItems;

	public void initilizeJobListItems() {
		jobListItems = new ArrayList<JobListItem>();
		for(WebElement item:jobListWebElements) {
			jobListItems.add(new JobListItem(item));
		}
	}

	public void searchForWorker(String workerId) {
		getWorkerIdInput().sendKeys(workerId);
		getSearchWorkerButton().click();
	}

	public void waitForJobList() {
		DriverManager.shortWait().until(ExpectedConditions.visibilityOf(getEmptyTableMessage()));
		DriverManager.shortWait().until(ExpectedConditions.not(new WebElementHasClass(getEmptyTableMessage(), "dataTables_empty")));
		initilizeJobListItems();
	}
	
	public void validate(WorkersApiWorker worker) {
		SoftAssertions softly = new SoftAssertions();
		
		softly.assertThat(getWorkerName().getAttribute("value")).as("Worker #%s: name ", worker.getUserId()).isEqualTo(worker.getName().toString());
		softly.assertThat(getWorkerAge().getAttribute("value")).as("Worker #%s: age ", worker.getUserId()).isEqualTo(worker.getAge()+"");
		softly.assertThat(getWorkerEmail().getAttribute("value")).as("Worker #%s: email ", worker.getUserId()).isEqualTo(worker.getEmail());
		softly.assertThat(getWorkerPhone().getAttribute("value")).as("Worker #%s: phone ", worker.getUserId()).isEqualTo(worker.getPhone());
		softly.assertThat(getWorkerIsActive().getAttribute("value")).as("Worker #%s: active ", worker.getUserId()).isEqualTo(worker.isActive()+"");
		
		softly.assertAll();
	}

	public WebElement getWorkerIdInput() {
		return workerIdInput;
	}

	public WebElement getSearchWorkerButton() {
		return searchWorkerButton;
	}

	public WebElement getWorkerName() {
		return workerName;
	}

	public WebElement getWorkerAge() {
		return workerAge;
	}

	public WebElement getWorkerEmail() {
		return workerEmail;
	}

	public WebElement getWorkerPhone() {
		return workerPhone;
	}

	public WebElement getWorkerRating() {
		return workerRating;
	}

	public WebElement getWorkerIsActive() {
		return workerIsActive;
	}

	public WebElement getJobSerachInput() {
		return jobSerachInput;
	}

	public WebElement getRankTableTitle() {
		return rankTableTitle;
	}

	public WebElement getJobTableTitle() {
		return jobTableTitle;
	}

	public WebElement getCompanyTableTite() {
		return companyTableTite;
	}

	public WebElement getBillRateTableTitle() {
		return billRateTableTitle;
	}

	public List<WebElement> getJobListWebElements() {
		return jobListWebElements;
	}

	public List<JobListItem> getJobListItems() {
		return jobListItems;
	}

	public WebElement getEmptyTableMessage() {
		return emptyTableMessage;
	}


}
