package niallconroy.swipejobs.serialise;

import java.util.Map;

import org.assertj.core.api.SoftAssertions;

public class GetWorkControllerWorker {
	boolean active;
	int age;
	String[] availability;
	String[] certificates;
	String email;
	String guid;
	boolean hasDriversLicence;
	boolean isActive;

	JobSearchAddress jobSearchAddress;
	WorkerName name;
	String phone;
	String[] skills;
	String transportation;
	int userId;

	public void validate() {
		SoftAssertions softly = new SoftAssertions();
		softly.assertThat(active).as("active").isNotNull();
		softly.assertThat(age).as("age").isNotNull();
		
		softly.assertThat(availability).as("availability").isNotNull();
		softly.assertThat(availability).as("availability").doesNotContainNull();
		softly.assertThat(certificates).as("certificates").isNotNull();
		softly.assertThat(certificates).as("certificates").doesNotContainNull();
		softly.assertThat(email).as("email").isNotNull();
		softly.assertThat(guid).as("guid").isNotNull();
		softly.assertThat(hasDriversLicence).as("hasDriversLicence").isNotNull();
		softly.assertThat(jobSearchAddress).as("jobSearchAddress").isNotNull();
		if(jobSearchAddress!=null)
			jobSearchAddress.validate();
		softly.assertThat(name).as("name").isNotNull();
		if(name!=null)
			name.validate(); 
		softly.assertThat(phone).as("phone").isNotNull();
		softly.assertThat(skills).as("skills").isNotNull();
		softly.assertThat(transportation).as("transportation").isNotNull();;
		softly.assertThat(userId).as("userId").isNotNull();
		softly.assertAll();
	}

	public void validateAgainst(WorkersApiWorker worker) {
		SoftAssertions softly = new SoftAssertions();
		softly.assertThat(active).as("Worker #%s: active", userId).isEqualTo(worker.getActive());
		softly.assertThat(age).as("Worker #%s: age", userId).isEqualTo(worker.getAge());

		for(Map<String, String> day :worker.getAvailability()) {
			softly.assertThat(day).as("Worker #%s: availability day from swipejobs backend service", userId).isNotNull();
			if(day!=null)
				softly.assertThat(availability).contains(day.get("title").toUpperCase());
		}

		softly.assertThat(certificates).as("Worker #%s: certificates", userId).isEqualTo(worker.getCertificates());
		softly.assertThat(email).as("Worker #%s: email", userId).isEqualTo(worker.getEmail());
		softly.assertThat(guid).as("Worker #%s: guid", userId).isEqualTo(worker.getGuid());
		softly.assertThat(hasDriversLicence).as("Worker #%s: hasDriversLicence", userId).isEqualTo(worker.isHasDriversLicence());
		softly.assertThat(jobSearchAddress).as("Worker #%s: jobSearchAddress", userId).isNotNull();
		softly.assertThat(worker.getJobSearchAddress()).as("Worker #%s: jobSearchAddress from swipejob service", userId).isNotNull();
		if(jobSearchAddress!=null && worker.getJobSearchAddress()!=null) {
			softly.assertThat(jobSearchAddress.getLatitude()).as("Worker #%s: jobSearchAddress.getLatitude", userId).isEqualTo(worker.getJobSearchAddress().getLatitude());;
			softly.assertThat(jobSearchAddress.getLongitude()).as("Worker #%s: jobSearchAddress.getLongitude", userId).isEqualTo(worker.getJobSearchAddress().getLongitude());;
			softly.assertThat(jobSearchAddress.getUnit()).as("Worker #%s: jobSearchAddress.getUnit", userId).isEqualTo(worker.getJobSearchAddress().getUnit());;
			softly.assertThat(jobSearchAddress.getMaxJobDistance()).as("Worker #%s: jobSearchAddress.getMaxJobDistance", userId).isEqualTo(worker.getJobSearchAddress().getMaxJobDistance());;
		}
		
		softly.assertThat(name).as("Worker #%s: name", userId).isNotNull();
		softly.assertThat(worker.getName()).as("Worker #%s: name from swipejob service", userId).isNotNull();
		if(name!=null && worker.getName()!=null) {
			softly.assertThat(name.getFirst()).as("Worker #%s: name.getFirst", userId).isEqualTo(worker.getName().getFirst());;
			softly.assertThat(name.getLast()).as("Worker #%s: name.getLast", userId).isEqualTo(worker.getName().getLast());;
		}

		softly.assertThat(phone).as("Worker #%s: phone", userId).isEqualTo(worker.getPhone());
		softly.assertThat(skills).as("Worker #%s: skills", userId).isEqualTo(worker.getSkills());
		softly.assertThat(transportation).as("Worker #%s: transportation", userId).isEqualTo(worker.getTransportation());
		softly.assertThat(userId).as("Worker #%s: userId", userId).isEqualTo(worker.getUserId());
		softly.assertAll();
	}

	public boolean getActive() {
		return active;
	}
	public int getAge() {
		return age;
	}
	public String[] getAvailability() {
		return availability;
	}
	public String[] getCertificates() {
		return certificates;
	}
	public String getEmail() {
		return email;
	}
	public String getGuid() {
		return guid;
	}
	public boolean isHasDriversLicence() {
		return hasDriversLicence;
	}
	public boolean isActive() {
		return isActive;
	}
	public JobSearchAddress getJobSearchAddress() {
		return jobSearchAddress;
	}
	public WorkerName getName() {
		return name;
	}
	public String getPhone() {
		return phone;
	}
	public String[] getSkills() {
		return skills;
	}
	public String getTransportation() {
		return transportation;
	}
	public int getUserId() {
		return userId;
	}

}
