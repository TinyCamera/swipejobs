package niallconroy.swipejobs.serialise;

import org.assertj.core.api.SoftAssertions;

public class Job {
	String about;
	String billrate;
	String company;
	boolean driverLicenseRequired;
	String guid;
	int jobId;
	String jobTitle;
	Location location;
	int pointsReceived;
	String[] requiredCertificates;
	String startDate;
	int workersRequired;
	
	public void validate() {
		SoftAssertions softly = new SoftAssertions();
		softly.assertThat(about).as("about").isNotNull();
		softly.assertThat(billrate).as("billrate").isNotNull();
		softly.assertThat(company).as("company").isNotNull();
		softly.assertThat(driverLicenseRequired).as("driverLicenseRequired").isNotNull();
		softly.assertThat(guid).as("guid").isNotNull();
		softly.assertThat(jobId).as("jobId").isNotNull();
		softly.assertThat(jobTitle).as("jobTitle").isNotNull();
		softly.assertThat(location).as("location").isNotNull();
		if(location!=null)
			location.validate();
		softly.assertThat(pointsReceived).as("pointsReceived").isNotNull();
		softly.assertThat(requiredCertificates).as("requiredCertificates").isNotNull();
		softly.assertThat(requiredCertificates).as("requiredCertificates").doesNotContainNull();
		softly.assertThat(startDate).as("startDate").isNotNull();
		softly.assertThat(workersRequired).as("workersRequired").isNotNull();
	}

	public String getAbout() {
		return about;
	}

	public String getBillrate() {
		return billrate;
	}

	public String getCompany() {
		return company;
	}

	public boolean isDriverLicenseRequired() {
		return driverLicenseRequired;
	}

	public String getGuid() {
		return guid;
	}

	public int getJobId() {
		return jobId;
	}

	public String getJobTitle() {
		return jobTitle;
	}

	public Location getLocation() {
		return location;
	}

	public int getPointsReceived() {
		return pointsReceived;
	}

	public String[] getRequiredCertificates() {
		return requiredCertificates;
	}

	public String getStartDate() {
		return startDate;
	}

	public int getWorkersRequired() {
		return workersRequired;
	}
	
	
}
