package niallconroy.swipejobs.serialise;

import org.assertj.core.api.SoftAssertions;

public class JobSearchAddress {
	String latitude;
	String longitude;
	int maxJobDistance;
	String unit;



	public void validate() {
		SoftAssertions softly = new SoftAssertions();
		softly.assertThat(latitude).as("lattitude").isNotNull();
		softly.assertThat(longitude).as("longitude").isNotNull();
		softly.assertThat(maxJobDistance).as("maxJobDistance").isNotNull();
		softly.assertThat(unit).as("unit").isNotNull();
		softly.assertAll();
	}


	public String getLatitude() {
		return latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public int getMaxJobDistance() {
		return maxJobDistance;
	}

	public String getUnit() {
		return unit;
	}


}
