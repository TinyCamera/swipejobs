package niallconroy.swipejobs.serialise;

import org.assertj.core.api.SoftAssertions;

public class Location {
	String latitude;
	String longitude;
	
	public void validate() {
		SoftAssertions softly = new SoftAssertions();
		softly.assertThat(latitude).as("latitude").isNotNull();
		softly.assertThat(longitude).as("longitude").isNotNull();
	}

	public String getLatitude() {
		return latitude;
	}

	public String getLongitude() {
		return longitude;
	}
	
	
}
