package niallconroy.swipejobs.serialise;

import org.assertj.core.api.SoftAssertions;

public class WorkerName {
	String first;
	String last;
	
	public void validate() {
		SoftAssertions softly = new SoftAssertions();
		softly.assertThat(first).as("first").isNotNull();
		softly.assertThat(last).as("last").isNotNull();
		softly.assertAll();
	}
	

	public String getFirst() {
		return first;
	}

	public String getLast() {
		return last;
	}
	
	public String toString() {
		return String.format("%s %s", first, last);
	}
}
