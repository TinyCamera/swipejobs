package niallconroy.swipejobs.serialise;

import java.util.Map;

import org.assertj.core.api.SoftAssertions;

public class WorkersApiWorker {
	boolean active;
	int age;
	Map<String, String>[] availability;
	String[] certificates;
	String email;
	String guid;
	boolean hasDriversLicence;
	boolean isActive;
	
	JobSearchAddress jobSearchAddress;
	WorkerName name;
	String phone;
	String[] skills;
	String transportation;
	int userId;
	
	public void validate() {
		SoftAssertions softly = new SoftAssertions();
		softly.assertThat(active).as("active").isNotNull();
		softly.assertThat(age).as("age").isNotNull();

		softly.assertThat(availability).as("availability").isNotNull();

		softly.assertThat(availability).as("availability").doesNotContainNull();
		softly.assertThat(certificates).as("certificates").isNotNull();
		softly.assertThat(certificates).as("certificates").doesNotContainNull();
		softly.assertThat(email).as("email").isNotNull();
		softly.assertThat(guid).as("guid").isNotNull();
		softly.assertThat(hasDriversLicence).as("hasDriversLicence").isNotNull();
		softly.assertThat(jobSearchAddress).as("jobSearchAddress").isNotNull();
		if(jobSearchAddress!=null)
			jobSearchAddress.validate();
		softly.assertThat(name).as("name").isNotNull();
		if(name!=null)
			name.validate();
		softly.assertThat(phone).as("phone").isNotNull();
		softly.assertThat(skills).as("skills").isNotNull();
		softly.assertThat(transportation).as("transportation").isNotNull();;
		softly.assertThat(userId).as("userId").isNotNull();
		softly.assertAll();
	}
	
	public boolean getActive() {
		return active;
	}
	public int getAge() {
		return age;
	}
	public Map<String, String>[] getAvailability() {
		return availability;
	}
	public String[] getCertificates() {
		return certificates;
	}
	public String getEmail() {
		return email;
	}
	public String getGuid() {
		return guid;
	}
	public boolean isHasDriversLicence() {
		return hasDriversLicence;
	}
	public boolean isActive() {
		return isActive;
	}
	public JobSearchAddress getJobSearchAddress() {
		return jobSearchAddress;
	}
	public WorkerName getName() {
		return name;
	}
	public String getPhone() {
		return phone;
	}
	public String[] getSkills() {
		return skills;
	}
	public String getTransportation() {
		return transportation;
	}
	public int getUserId() {
		return userId;
	}
	
}
