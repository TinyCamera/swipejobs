package niallconroy.swipejobs.testconfig;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;

import niallconroy.swipejobs.test.SeleniumTest;
import niallconroy.swipejobs.webdriver.DriverManager;

public abstract class SwipeJobFrontEndTest extends SeleniumTest{

	@Parameters("swipeJobsURL")
	@BeforeMethod()
	public void loadSwipeJobsWebPage(String swipeJobsURL) {
		DriverManager.get().get(swipeJobsURL + "/swipeJobsAPI");
	}

}
