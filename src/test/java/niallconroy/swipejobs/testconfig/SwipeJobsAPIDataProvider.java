package niallconroy.swipejobs.testconfig;

import static com.jayway.restassured.RestAssured.get;

import java.util.HashMap;
import java.util.Map;

import org.testng.ITestContext;
import org.testng.annotations.DataProvider;

import com.jayway.restassured.RestAssured;

import niallconroy.swipejobs.api.RestAssuredSerialiser;
import niallconroy.swipejobs.serialise.Job;
import niallconroy.swipejobs.serialise.WorkersApiWorker;
import niallconroy.swipejobs.test.TestBase;

/**
 * To allow broad test coverage with regard data, i created 2 data providers that read from the jobs and workers api
 * These data providers can be used for integration tests but also to validate their respective apis 
 * @author Niall
 *
 */
public class SwipeJobsAPIDataProvider extends TestBase {

	public static final String WORKER_JOB_DATA_PROVIDER = "getWorkerData";
	public static final String JOB_DATA_PROVIDER = "getJobData";

	public static WorkersApiWorker[] workers;
	public static Job[] jobs;

	@DataProvider(name=WORKER_JOB_DATA_PROVIDER)
	public Object[][] getWorkerData(ITestContext context){
		String swipeJobsServicesURL = context.getCurrentXmlTest().getParameter("swipeJobsServicesURL");
		RestAssured.baseURI = swipeJobsServicesURL;

		if(workers==null)
			workers = RestAssuredSerialiser.serialise(get("workers"), WorkersApiWorker[].class);
		if(jobs==null)
			jobs = RestAssuredSerialiser.serialise(get("jobs"), Job[].class);

		Map<Integer, Job> jobDataMap = new HashMap<Integer, Job>();
		
		for(Job j: jobs) {
			jobDataMap.put(j.getJobId(), j);
		}
		
		Object [][] data = new Object[workers.length][];
		int count = 0;
		for(WorkersApiWorker w: workers) {
			Object[] dataArray = new Object[2];
			dataArray[0] = w;
			dataArray[1] = jobDataMap;
			data[count++] = dataArray;
		}

		return data;
	}

	@DataProvider(name=JOB_DATA_PROVIDER)
	public Object[][] getJobData(ITestContext context){
		String swipeJobsServicesURL = context.getCurrentXmlTest().getParameter("swipeJobsServicesURL");
		RestAssured.baseURI = swipeJobsServicesURL;
		
		if(jobs==null)
			jobs = RestAssuredSerialiser.serialise(get("jobs"), Job[].class);


		Object [][] data = new Object[jobs.length][];
		int count = 0;
		for(Job j: jobs) {
			Job[] jobArray = new Job[1];
			jobArray[0] = j;
			data[count++] = jobArray;
		}

		return data;
	}
}

