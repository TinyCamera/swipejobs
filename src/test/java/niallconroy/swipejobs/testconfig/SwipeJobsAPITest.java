package niallconroy.swipejobs.testconfig;

import org.testng.annotations.BeforeMethod;	
import org.testng.annotations.Parameters;

import com.jayway.restassured.RestAssured;

import niallconroy.swipejobs.test.TestBase;

public abstract class SwipeJobsAPITest extends TestBase{

	protected static final String WORK_CONTROLLER = "getWorkController";
	protected static final String JOBS_BY_WORKER = "restBestRankedJobsByWorker";

	protected static final String USER_ID = "userId";

	@Parameters("swipeJobsURL")
	@BeforeMethod
	public void setup(String swipeJobsURL) {
		RestAssured.baseURI  = swipeJobsURL;
	}


}
