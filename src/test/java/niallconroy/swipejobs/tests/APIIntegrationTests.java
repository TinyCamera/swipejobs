package niallconroy.swipejobs.tests;

import static com.jayway.restassured.RestAssured.given;

import java.util.Map;

import org.assertj.core.api.SoftAssertions;
import org.testng.annotations.Test;

import com.jayway.restassured.response.Response;

import niallconroy.swipejobs.api.RestAssuredSerialiser;
import niallconroy.swipejobs.serialise.GetWorkControllerWorker;
import niallconroy.swipejobs.serialise.Job;
import niallconroy.swipejobs.serialise.WorkersApiWorker;
import niallconroy.swipejobs.testconfig.SwipeJobsAPIDataProvider;
import niallconroy.swipejobs.testconfig.SwipeJobsAPITest;

public class APIIntegrationTests extends SwipeJobsAPITest{

	@Test(dataProviderClass=SwipeJobsAPIDataProvider.class, dataProvider=SwipeJobsAPIDataProvider.WORKER_JOB_DATA_PROVIDER, groups= {"workControllerIntegrationTests"}, description="Verify data from the getWorkController api makes the workers api")
	public void workControllerIntegrationTests(WorkersApiWorker worker, Map<Integer, Job> jobDataMap) {
		Response response = given().queryParam(USER_ID, worker.getUserId()).when()
				.post(WORK_CONTROLLER);
		response.then().statusCode(200); 

		GetWorkControllerWorker workerFromSwipeJobsAPI = RestAssuredSerialiser.serialise(response, GetWorkControllerWorker.class);

		SoftAssertions softly = new SoftAssertions();
		validateWorker(workerFromSwipeJobsAPI, worker, softly);
		softly.assertAll(); 
	}


	@Test(dataProviderClass=SwipeJobsAPIDataProvider.class, dataProvider=SwipeJobsAPIDataProvider.WORKER_JOB_DATA_PROVIDER, groups= {"jobByWorkerIntegrationsTests"}, description="Verify data from the restBestRankedJobsByWorker api matches that from the jobs api ")
	public void jobByWorkerIntegrationsTests(WorkersApiWorker worker, Map<Integer, Job> jobDataMap) {
		Response response = given().queryParam(USER_ID, worker.getUserId()).when()
				.get(JOBS_BY_WORKER);
		response.then().statusCode(200); 


		Job[] rankedJobs = RestAssuredSerialiser.serialise(response, Job[].class);

		SoftAssertions softly = new SoftAssertions();
		validateJobs(worker.getUserId(),rankedJobs, jobDataMap, softly);
		softly.assertAll();
	}

	private void validateWorker(GetWorkControllerWorker worker, WorkersApiWorker swipeJobWorker, SoftAssertions softly) {
		softly.assertThat(worker.isActive()).as("Worker #%s: active", worker.getUserId()).isEqualTo(swipeJobWorker.getActive());
		softly.assertThat(worker.getAge()).as("Worker #%s: age", worker.getUserId()).isEqualTo(swipeJobWorker.getAge());

		for(Map<String, String> day :swipeJobWorker.getAvailability()) {
//			softly.assertThat(day).as("Worker #%s: availability day from swipejobs backend service", worker.getUserId()).isNotNull();
			if(day!=null)
				softly.assertThat(worker.getAvailability()).contains(day.get("title").toUpperCase());
		}

		softly.assertThat(worker.getCertificates()).as("Worker #%s: certificates", worker.getUserId()).isEqualTo(swipeJobWorker.getCertificates());
		softly.assertThat(worker.getEmail()).as("Worker #%s: email", worker.getUserId()).isEqualTo(swipeJobWorker.getEmail());
		softly.assertThat(worker.getGuid()).as("Worker #%s: guid", worker.getUserId()).isEqualTo(swipeJobWorker.getGuid());
		softly.assertThat(worker.isHasDriversLicence()).as("Worker #%s: hasDriversLicence", worker.getUserId()).isEqualTo(swipeJobWorker.isHasDriversLicence());
		softly.assertThat(worker.getJobSearchAddress()).as("Worker #%s: jobSearchAddress", worker.getUserId()).isNotNull();
		softly.assertThat(swipeJobWorker.getJobSearchAddress()).as("Worker #%s: jobSearchAddress from swipejob service", worker.getUserId()).isNotNull();
		if(worker.getJobSearchAddress()!=null && swipeJobWorker.getJobSearchAddress()!=null) {
			softly.assertThat(worker.getJobSearchAddress().getLatitude()).as("Worker #%s: jobSearchAddress.getLatitude", worker.getUserId()).isEqualTo(swipeJobWorker.getJobSearchAddress().getLatitude());;
			softly.assertThat(worker.getJobSearchAddress().getLongitude()).as("Worker #%s: jobSearchAddress.getLongitude", worker.getUserId()).isEqualTo(swipeJobWorker.getJobSearchAddress().getLongitude());;
			softly.assertThat(worker.getJobSearchAddress().getUnit()).as("Worker #%s: jobSearchAddress.getUnit", worker.getUserId()).isEqualTo(swipeJobWorker.getJobSearchAddress().getUnit());;
			softly.assertThat(worker.getJobSearchAddress().getMaxJobDistance()).as("Worker #%s: jobSearchAddress.getMaxJobDistance", worker.getUserId()).isEqualTo(swipeJobWorker.getJobSearchAddress().getMaxJobDistance());;
		}
		softly.assertThat(worker.getName()).as("Worker #%s: name", worker.getUserId()).isNotNull();
		softly.assertThat(swipeJobWorker.getName()).as("Worker #%s: name from swipejob service", worker.getUserId()).isNotNull();
		if(worker.getName()!=null && swipeJobWorker.getName()!=null) {
			softly.assertThat(worker.getName().getFirst()).as("Worker #%s: name.getFirst", worker.getUserId()).isEqualTo(swipeJobWorker.getName().getFirst());;
			softly.assertThat(worker.getName().getLast()).as("Worker #%s: name.getLast", worker.getUserId()).isEqualTo(swipeJobWorker.getName().getLast());;
		}
		softly.assertThat(worker.getPhone()).as("Worker #%s: phone", worker.getUserId()).isEqualTo(swipeJobWorker.getPhone());
		softly.assertThat(worker.getSkills()).as("Worker #%s: skills", worker.getUserId()).isEqualTo(swipeJobWorker.getSkills());
		softly.assertThat(worker.getTransportation()).as("Worker #%s: transportation", worker.getUserId()).isEqualTo(swipeJobWorker.getTransportation());
		softly.assertThat(worker.getUserId()).as("Worker #%s: worker.getUserId()", worker.getUserId()).isEqualTo(swipeJobWorker.getUserId());
	}

	private void validateJobs(int workerId, Job[] rankedJobs, Map<Integer, Job> jobDataMap, SoftAssertions softly) {
		int rank =0;
		for(Job job: rankedJobs) {
			Job matchingBackendJob = jobDataMap.get(job.getJobId());

			softly.assertThat(job.getAbout()).as("Worker %s, Job rank:%s Id:%s - about", workerId, rank, job.getJobId()).isEqualTo(matchingBackendJob.getAbout());
			softly.assertThat(job.getBillrate()).as("Worker %s, Job rank:%s Id:%s - billrate",workerId,  rank, job.getJobId()).isEqualTo(matchingBackendJob.getBillrate());
			softly.assertThat(job.getCompany()).as("Worker %s, Job rank:%s Id:%s - company", workerId, rank, job.getJobId()).isEqualTo(matchingBackendJob.getCompany());
			softly.assertThat(job.getGuid()).as("Worker %s, Job rank:%s Id:%s - GUID", workerId, rank, job.getJobId()).isEqualTo(matchingBackendJob.getGuid());
			softly.assertThat(job.getJobTitle()).as("Worker %s, Job rank:%s Id:%s - job title", workerId, rank, job.getJobId()).isEqualTo(matchingBackendJob.getJobTitle());
		
			//Hackily handle slightly different start date formats, should use dateformat probably
			String startDate = matchingBackendJob.getStartDate();
			//remove trailing Z
			startDate = startDate.substring(0, startDate.length()-1);

			//add the trailing zeros
			while(startDate.length()< 23) {
				startDate+="0";
			}
			
			softly.assertThat(job.getStartDate()).as("Worker %s, Job rank:%s Id:%s - start date",workerId,  rank, job.getJobId()).isEqualTo(startDate);
			softly.assertThat(job.getWorkersRequired()).as("Worker %s, Job rank:%s Id:%s - workers required",workerId,  rank, job.getJobId()).isEqualTo(matchingBackendJob.getWorkersRequired());
			softly.assertThat(job.isDriverLicenseRequired()).as("Worker %s, Job rank:%s Id:%s - driver licence required", rank, job.getJobId()).isEqualTo(matchingBackendJob.isDriverLicenseRequired());

			softly.assertThat(job.getLocation()).as("Worker %s, Job rank:%s Id:%s - JobByWorker locatation", workerId, rank, job.getJobId()).isNotNull();
			softly.assertThat(matchingBackendJob.getLocation()).as("Worker %s, Job rank:%s Id:%s - Jobs api locatation", workerId, rank, job.getJobId()).isNotNull();
			if(job.getLocation()!=null&&matchingBackendJob.getLocation()!=null) {
				softly.assertThat(job.getLocation().getLatitude()).as("Worker %s, Job rank:%s Id:%s - Locations:latitue", workerId, rank, job.getJobId()).isEqualTo(matchingBackendJob.getLocation().getLatitude());
				softly.assertThat(job.getLocation().getLongitude()).as("Worker %s, Job rank:%s Id:%s - Locations:longitude",workerId,  rank, job.getJobId()).isEqualTo(matchingBackendJob.getLocation().getLongitude());
			}

			softly.assertThat(job.getRequiredCertificates()).as("Worker %s, Job rank:%s Id:%s - required certificates", workerId, rank, job.getJobId()).containsExactly(matchingBackendJob.getRequiredCertificates());

		}
	}
}
