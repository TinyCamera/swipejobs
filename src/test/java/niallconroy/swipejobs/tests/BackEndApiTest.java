package niallconroy.swipejobs.tests;

import java.util.Map;

import org.testng.annotations.Test;

import niallconroy.swipejobs.serialise.Job;
import niallconroy.swipejobs.serialise.WorkersApiWorker;
import niallconroy.swipejobs.testconfig.SwipeJobsAPIDataProvider;

public class BackEndApiTest extends SwipeJobsAPIDataProvider{
	
	@Test(dataProvider=WORKER_JOB_DATA_PROVIDER, groups= {"workerapivalidation"}, description="Validates that data from the workers api is in correct format")
	public void validateBackEndWorkerAPITest_102(WorkersApiWorker worker, Map<Integer, Job> jobData) {
		worker.validate();
	}
	
	@Test(dataProvider=JOB_DATA_PROVIDER, groups= {"jobapivalidation"}, description="Validates that data from the workers api is in correct format")
	public void validateBackEndJobAPITest_103(Job job) {
		job.validate();
	}

}
