package niallconroy.swipejobs.tests;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.List;

import org.assertj.core.api.SoftAssertions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import niallconroy.swipejobs.pages.JobListItem;
import niallconroy.swipejobs.pages.SwipeJobsPage;
import niallconroy.swipejobs.testconfig.SwipeJobFrontEndTest;
import niallconroy.swipejobs.webdriver.DriverManager;

public class FrontEndFunctionalTest extends SwipeJobFrontEndTest{
	
	@Test(groups= {"frontendFunctional"}, description="Verify that after search for a worker, worker data fields are populated")
	public void searchForWorkerPopulatesWorkerDetails_001() {
		SwipeJobsPage swipeJobsPage = new SwipeJobsPage();

		swipeJobsPage.searchForWorker("1");

		DriverManager.shortWait().until(ExpectedConditions.attributeToBeNotEmpty(swipeJobsPage.getWorkerName(), "value"));

		SoftAssertions softly = new SoftAssertions();

		softly.assertThat(swipeJobsPage.getWorkerName().getAttribute("value")).as("Worker name").isNotEmpty();
		softly.assertThat(swipeJobsPage.getWorkerAge().getAttribute("value")).as("Worker age").isNotEmpty();
		softly.assertThat(swipeJobsPage.getWorkerEmail().getAttribute("value")).as("Worker email").isNotEmpty();
		softly.assertThat(swipeJobsPage.getWorkerPhone().getAttribute("value")).as("Worker phone").isNotEmpty();
		softly.assertThat(swipeJobsPage.getWorkerRating().getAttribute("value")).as("Worker rating").isNotEmpty();
		softly.assertThat(swipeJobsPage.getWorkerIsActive().getAttribute("value")).as("Worker Is Active").isNotEmpty();
		softly.assertAll();

	}

	@Test(groups= {"frontendFunctional"}, description="Verify that after search for a worker, ranked jobs list is populated")
	public void searchForWorkerPopulatesJobsList_002() {
		SwipeJobsPage swipeJobsPage = new SwipeJobsPage();
		swipeJobsPage.searchForWorker("1");
		swipeJobsPage.waitForJobList();

		assertThat(swipeJobsPage.getJobListItems()).as("Job List Items").hasSize(3);

		SoftAssertions softly = new SoftAssertions();
		int itemNum = 0;
		for(JobListItem item: swipeJobsPage.getJobListItems()) {
			itemNum++;
			softly.assertThat(item.getRank().getText()).as("Item %s: rank", itemNum).isEqualTo(itemNum);
			softly.assertThat(item.getJobTitle().getText()).as("Item %s: job title", itemNum).isNotEmpty();
			softly.assertThat(item.getCompany().getText()).as("Item %s: company", itemNum).isNotEmpty();
			softly.assertThat(item.getBillRate().getText()).as("Item %s: bill rate", itemNum).isNotEmpty();
		}

		softly.assertAll();

	}


	@Test(groups= {"frontendFunctional"}, description="Verify you can search from jobs within the ranked jobs list")
	public void jobListSearchTest_003() {
		SwipeJobsPage swipeJobsPage = new SwipeJobsPage();
		swipeJobsPage.searchForWorker("1");
		swipeJobsPage.waitForJobList();

		String jobTitle = swipeJobsPage.getJobListItems().get(0).getJobTitle().getText();
		swipeJobsPage.getJobSerachInput().sendKeys(jobTitle);

		swipeJobsPage.initilizeJobListItems();

		assertThat(swipeJobsPage.getJobListItems().size()).as("Number of items in job list").isGreaterThan(0);

		SoftAssertions softly = new SoftAssertions();
		int itemNum = 0;
		for(JobListItem item: swipeJobsPage.getJobListItems()) {
			itemNum++;
			softly.assertThat(item.getJobTitle().getText()).as("Item %s: job title", itemNum).contains(jobTitle);
		}
		softly.assertAll();
	}

	@DataProvider(name="invalidWorkerSearchTerms")
	public static Object[][] invalidWorkerSearchTerms() {
		return new Object[][] { {""}, { "-1" }, { "9999"}, {"invalid search term"}}; 
	}

	@Test(groups= {"frontendFunctional"}, dataProvider = "invalidWorkerSearchTerms", description="Verfiy that entering an invalid serach team, no results are returned")
	public void incorrectWorkerSearchTermTest_004(String searchTerm) {
		SwipeJobsPage swipeJobsPage = new SwipeJobsPage();
		swipeJobsPage.searchForWorker(searchTerm);

		SoftAssertions softly = new SoftAssertions();

		softly.assertThat(swipeJobsPage.getWorkerName().getAttribute("value")).as("Worker name").isEmpty();
		softly.assertThat(swipeJobsPage.getWorkerAge().getAttribute("value")).as("Worker age").isEmpty();
		softly.assertThat(swipeJobsPage.getWorkerEmail().getAttribute("value")).as("Worker email").isEmpty();
		softly.assertThat(swipeJobsPage.getWorkerPhone().getAttribute("value")).as("Worker phone").isEmpty();
		softly.assertThat(swipeJobsPage.getWorkerRating().getAttribute("value")).as("Worker rating").isEmpty();
		softly.assertThat(swipeJobsPage.getWorkerIsActive().getAttribute("value")).as("Worker Is Active").isEmpty();
		softly.assertAll();

	}

	@Test(groups= {"frontendFunctional"}, description="Verify when you search for a job that is not in the ranked job list, an error is shown")
	public void incorrectJobTableSearchTerms_005() {
		SwipeJobsPage swipeJobsPage = new SwipeJobsPage();
		swipeJobsPage.searchForWorker("1");
		swipeJobsPage.waitForJobList();

		swipeJobsPage.getJobSerachInput().sendKeys("INVALID SEARCH TERM");

		assertThat(swipeJobsPage.getEmptyTableMessage().getText()).as("Table message").isEqualTo("No matching records found");

	}


	@Test(groups= {"frontendFunctional"}, description="Verify when you search for worker, the ranked job list is sorted by rank")
	public void tableIsSortedTest_006() {
		SwipeJobsPage swipeJobsPage = new SwipeJobsPage();
		swipeJobsPage.searchForWorker("1");
		swipeJobsPage.waitForJobList();


		List<Integer> ranks = new ArrayList<Integer>();
		
		for(JobListItem item: swipeJobsPage.getJobListItems()) {
			ranks.add(Integer.parseInt(item.getRank().getText()));
		}
		
		assertThat(ranks).as("").isSorted();
	}

	@Test(groups= {"frontendFunctional"}, description="Verify that when a search for a worker is failed, previous worker details are cleared")
	public void searchNewWorkerNegitiveTest_007() {
		SwipeJobsPage swipeJobsPage = new SwipeJobsPage();
		swipeJobsPage.searchForWorker("1");
		swipeJobsPage.waitForJobList();

		swipeJobsPage.searchForWorker("-1");
		swipeJobsPage.waitForJobList();
		
		SoftAssertions softly = new SoftAssertions();

		softly.assertThat(swipeJobsPage.getWorkerName().getAttribute("value")).as("Worker name").isEmpty();
		softly.assertThat(swipeJobsPage.getWorkerAge().getAttribute("value")).as("Worker age").isEmpty();
		softly.assertThat(swipeJobsPage.getWorkerEmail().getAttribute("value")).as("Worker email").isEmpty();
		softly.assertThat(swipeJobsPage.getWorkerPhone().getAttribute("value")).as("Worker phone").isEmpty();
		softly.assertThat(swipeJobsPage.getWorkerRating().getAttribute("value")).as("Worker rating").isEmpty();
		softly.assertThat(swipeJobsPage.getWorkerIsActive().getAttribute("value")).as("Worker Is Active").isEmpty();
		
		softly.assertThat(swipeJobsPage.getJobListWebElements()).as("Job list").hasSize(1);
		softly.assertThat(swipeJobsPage.getEmptyTableMessage().getText()).as("Table message").isEqualTo("No data available in table");
		
		softly.assertAll();
	
	}
}
