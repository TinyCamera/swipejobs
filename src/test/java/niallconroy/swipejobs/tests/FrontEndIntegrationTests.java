package niallconroy.swipejobs.tests;

import java.util.Map;

import org.testng.annotations.Test;

import niallconroy.swipejobs.pages.SwipeJobsPage;
import niallconroy.swipejobs.serialise.Job;
import niallconroy.swipejobs.serialise.WorkersApiWorker;
import niallconroy.swipejobs.testconfig.SwipeJobFrontEndTest;
import niallconroy.swipejobs.testconfig.SwipeJobsAPIDataProvider;

public class FrontEndIntegrationTests  extends SwipeJobFrontEndTest{

	@Test(groups= {"frontendIntegrationTests"}, dataProviderClass=SwipeJobsAPIDataProvider.class, dataProvider=SwipeJobsAPIDataProvider.WORKER_JOB_DATA_PROVIDER, description="Validate data retrieved from the frontend against the workers api")
	public void frontEndIntegrationTEst_104(WorkersApiWorker worker, Map<Integer, Job> jobData) {
		SwipeJobsPage swipeJobsPage = new SwipeJobsPage();
		swipeJobsPage.searchForWorker(worker.getUserId()+"");
		swipeJobsPage.waitForJobList();

		swipeJobsPage.validate(worker);

	}

}
