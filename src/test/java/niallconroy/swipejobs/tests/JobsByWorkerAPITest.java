package niallconroy.swipejobs.tests;

import static com.jayway.restassured.RestAssured.given;
import static org.assertj.core.api.Assertions.assertThat;

import java.util.Arrays;

import org.testng.annotations.Test;

import com.jayway.restassured.response.Response;

import niallconroy.swipejobs.api.RestAssuredSerialiser;
import niallconroy.swipejobs.serialise.Job;
import niallconroy.swipejobs.testconfig.SwipeJobsAPITest;

public class JobsByWorkerAPITest extends SwipeJobsAPITest {

	@Test(groups = {"functionalApiTest"}, description="Verfify that restBestRankedJobsByWorker api returns an 400 error code when no userId param is passed")
	public void jobsByWorkerNoParamNegitiveTest_012() {
		given().when().get(JOBS_BY_WORKER).then().statusCode(400).statusLine("Bad Request");
	}


	@Test(groups = {"functionalApiTest"}, description="Verfify that when search for a invalid worker, an 400 error code is returned")
	public void jobsByWorkerNegitiveIntParamNegitiveTest_013() {
		given().queryParam(USER_ID, -1).when()
		.get(JOBS_BY_WORKER)
		.then().statusCode(400).statusLine("Bad Request");
	}
	
	@Test(groups = {"functionalApiTest"}, description="Verfify that when search for a null userId, an 400 error code is returned")
	public void jobsByWorkerNullParamNegitiveTest_013() {
		given().queryParam(USER_ID, "").when()
		.get(JOBS_BY_WORKER)
		.then().statusCode(400).statusLine("Bad Request");
	}

	@Test(groups = {"functionalApiTest"}, description="Verfify that when searchign for a worker that is not found, an 404 error code is returned")
	public void jobsByWorkerWorkerNotFoundNegitiveTest_014() {
		given().queryParam(USER_ID, 9999).when()
		.get(JOBS_BY_WORKER)
		.then().statusCode(404);
	}

	@Test(groups = {"functionalApiTest"}, description="Verfify that when searching for a user, the resposne is in the correct format, and 3 jobs are returned")
	public void jobsByWorkerTest_015() {
		Response response = given().queryParam(USER_ID, 1).when()
		.get(JOBS_BY_WORKER);
		
		response.then().statusCode(200);
		
		Job jobs[] = RestAssuredSerialiser.serialise(response, Job[].class);
		
		Arrays.stream(jobs).forEach(j -> {
			j.validate();
		});;

		assertThat(jobs).as("Ranked Jobs arrays").hasSize(3);
	}

	@Test(groups = {"functionalApiTest"}, description="Verfify that making a request with a userId param in the wrong format, an 400 error code is returned")
	public void jobsByWorkerInvalidParamNegitiveTest_016() {
		given().queryParam(USER_ID, "GARBAGE").when()
		.get(JOBS_BY_WORKER)
		.then().statusCode(400);
	}
}
