package niallconroy.swipejobs.tests;

import static com.jayway.restassured.RestAssured.given;

import org.testng.annotations.Test;

import com.jayway.restassured.response.Response;

import niallconroy.swipejobs.api.RestAssuredSerialiser;
import niallconroy.swipejobs.serialise.GetWorkControllerWorker;
import niallconroy.swipejobs.testconfig.SwipeJobsAPITest;

public class WorkControllerAPITests extends SwipeJobsAPITest{

	@Test(groups = {"functionalApiTest"}, description="Verfify when making a request to getWorkController api without the correct params, a 400 is returned")
	public void workControllerNoParamNegitiveTest_009() {
		given().when().post(WORK_CONTROLLER).then().statusCode(400);
	}

	@Test(groups = {"functionalApiTest"}, description="Verfify when making a request to getWorkController api with an invalid userId param, a 400 is returned")
	public void workControllerNegitiveIntParamNegitiveTest_010() {
		given().queryParam(USER_ID, -1).when()
		.post(WORK_CONTROLLER)
		.then().statusCode(400);
	}
	
	@Test(groups = {"functionalApiTest"}, description="Verfify when making a request to getWorkController api with an null userId param, a 400 is returned")
	public void workControllerNullParamNegitiveTest_010() {
		given().queryParam(USER_ID, "").when()
		.post(WORK_CONTROLLER)
		.then().statusCode(400);
	}

	@Test(groups = {"functionalApiTest"}, description="Verfify when making a request to getWorkController api, the resposne is in the correct format")
	public void workControllerTest_011() {
		Response response = given().queryParam(USER_ID, 1).when()
		.post(WORK_CONTROLLER);
		response.then().statusCode(200); 
		
		GetWorkControllerWorker worker= RestAssuredSerialiser.serialise(response, GetWorkControllerWorker.class);
		worker.validate();
	}


	@Test(groups = {"functionalApiTest"}, description="Verfify when making a request to getWorkController api with a userId param in an incorrect format, a 400 is returned")
	public void workControllerInvalidParamNegitiveTest_017() {
		given().queryParam(USER_ID, "GARBAGE").when()
		.get(WORK_CONTROLLER)
		.then().statusCode(400);
	}
}
